const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var GameSchema = new Schema({

	game_id: {
		type: String,
		required: true,
		unique: true
	}

},{_id: false})

GameSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Game', GameSchema);
