const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;
require('./gameModel');
var GameSchema = require('mongoose').model('Game').schema;


var ListSchema = new Schema({

    user_id: {
            type: String,
	    required: true,
	    unique: true
    },
    email: {
            type: String,
            unique: true,
            required: true,
            match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
    },
    wish: {
            type: [GameSchema]
    },
    owned: { 
            type: [GameSchema]
    }

});

ListSchema.plugin(uniqueValidator);
module.exports = mongoose.model('List', ListSchema);
