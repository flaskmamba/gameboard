const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;


let UserSchema = new Schema({

    username: {
            type: String,
            required: true,
            unique: true,
            minlength: 4,
            maxlength: 20,
            match: [/^\w*$/, 'Please fill a valid username (alphanumeric characters and underscore only)']
    },
    lastname: {
            type: String,
            match: [/^[a-zA-Z]*$/, 'Please fill a valid lastname (alphanumeric characters)'],
	    default: ''
    },
    firstname: {
            type: String,
            match: [/^[a-zA-Z]*$/, 'Please fill a valid firstname (alphanumeric characters)'],
	    default: ''
    },
    email: {
            type: String,
            index: true,
            unique: true,
            required: true,
            match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
    },
    password: {
            type: String,
            required: true
    },
    description: {
            type: String,
            maxlength: 2000,
	    default: ''
    },
    creation_date: {
            type: Date,
            default: Date.now 
    },
    birthday: {
            type: Date,
            default: null 
    },
    admin: {
            type: Boolean,
            default: false
    },
    ban: {
            type: Boolean,
            default: false
    }
});

UserSchema.methods.comparePassword = function(password) {
	return bcrypt.compareSync(password, this.password);
};
UserSchema.plugin(uniqueValidator);
module.exports = mongoose.model('User', UserSchema);

