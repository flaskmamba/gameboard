var assert = require('assert');
var chai = require('chai')
var chaiHttp = require('chai-http');
var expect = chai.expect;
var app = require('../app');
const mongoose = require('mongoose');
const User = require('../models/userModel');
const List = require('../models/listModel');

chai.use(chaiHttp);


describe('#USER', function() {

	before(function(done) {
		//Clean db
		mongoose.connect('mongodb://127.0.0.1:27017/GameBook', {useNewUrlParser: true});
		const connection = mongoose.connection;
		connection.once('open', function() {
		        console.log('MongoDB connection established successfully.')

			User.deleteOne({"email": "coco@lasticot.com"})
				.then(res => {
					List.find({"email": "coco@lasticot.com"})
						.then(res => {
							console.log('MongoDB cleaned before starting the tests.')
							done();
						});
				})
		});
	});

	after(function(done) {
		//Clean db
		mongoose.connect('mongodb://127.0.0.1:27017/GameBook', {useNewUrlParser: true});
		const connection = mongoose.connection;
		connection.once('open', function() {
		        console.log('MongoDB connection established successfully.')
			User.deleteOne({"email": "coco@lasticot.com"})
				.then(res => {
					List.deleteOne({"email": "coco@lasticot.com"})
						.then(res => {
							console.log('MongoDB cleaned before starting the tests.')
							done();
						});
				})
		});
	});

	describe('#Register', function() {
		it('should return status 200 and no err', (done) => {
			chai.request(app)
				.post('/register')
				.send({
					username: 'cocolasticot',
					email: 'coco@lasticot.com',
					password: '123456',
					password_confirm: '123456'
				})
				.end(function (err, res) {
					expect(err).to.be.null;
					expect(res).to.have.status(200);
					done();
					});
		});
	});

	describe('#Login', function() {
		it('should return status 200 and no err', (done) => {
			chai.request(app)
				.post('/login')
				.send({
					email: 'coco@lasticot.com',
					password: '123456',
				})
				.end(function (err, res) {
					expect(err).to.be.null;
					expect(res).to.have.status(200);
					done();
					});
		});
	});

	describe('#getUser', function() {
		it('should return status 200 and no err', (done) => {
			chai.request(app)
				.post('/login')
				.send({
					email: 'coco@lasticot.com',
					password: '123456',
				})
				.end(function (err, res) {
					token = res.body.token;
					chai.request(app)
						.post('/get/user')
						.send({
							token: token,
						})
						.end(function (err, res) {
							expect(err).to.be.null;
							expect(res).to.have.status(200);
							done();
							});
						});
		});
	});

	describe('#Update', function() {
		it('should return status 200 and no err', (done) => {
			chai.request(app)
				.post('/login')
				.send({
					email: 'coco@lasticot.com',
					password: '123456',
				})
				.end(function (err, res) {
					token = res.body.token;
					chai.request(app)
						.post('/update/user')
						.send({
							token: token,
							email: "coco@lasticot.com",
							new_username: "newcocolasticot",
							new_lastname: "lasticot",
							new_firstname: "coco",
							new_email: "coco@lasticot.com",
							new_password: "123456789",
							new_description: "Jesuisuneasticot",
							new_birthday: null
						})
						.end(function (err, res) {
							expect(err).to.be.null;
							expect(res).to.have.status(200);
							done();
							});
						});
		});
	});

	describe('#Delete', function() {
		it('should return status 200 and no err', (done) => {
			chai.request(app)
				.post('/login')
				.send({
					email: 'coco@lasticot.com',
					password: '123456789',
				})
				.end(function (err, res) {
					token = res.body.token;
					chai.request(app)
						.post('/delete/user')
						.send({
							token: token, 
							email: "coco@lasticot.com",
						})
						.end(function (err, res) {
							expect(err).to.be.null;
							expect(res).to.have.status(200);
							done();
							});
						});
		});
	});
});



//WISH LIST
describe('#WISH LIST', function() {
	before(function(done) {
		chai.request(app)
			.post('/register')
			.send({
				username: 'cocolasticot',
				email: 'coco@lasticot.com',
				password: '123456',
				password_confirm: '123456'
			})
			.end(function (err, res) {
				done();
				});
	});

	after(function(done) {
		//Clean db
		mongoose.connect('mongodb://127.0.0.1:27017/GameBook', {useNewUrlParser: true});
		const connection = mongoose.connection;
		connection.once('open', function() {
		        console.log('MongoDB connection established successfully.')
			User.deleteOne({"email": "coco@lasticot.com"})
				.then(res => {
					List.deleteOne({"email": "coco@lasticot.com"})
						.then(res => {
							console.log('MongoDB cleaned before starting the tests.')
							done();
						});
				})
		});
	});

	describe('#Add', function() {
		it('should return status 200 and no err', (done) => {
			chai.request(app)
				.post('/login')
				.send({
					email: 'coco@lasticot.com',
					password: '123456'
				})
				.end(function (err, res) {
					token = res.body.token;
					expect(err).to.be.null;
					expect(res).to.have.status(200);
					chai.request(app)
						.post('/add/game/wish')
						.send({
							token: token, 
							game_id: "321654987746516564",
						})
						.end(function (err, res) {
							expect(err).to.be.null;
							expect(res).to.have.status(200);
							done();
							});
				});
		});
	});

	describe('#Read', function() {
		it('should return status 200 and no err', (done) => {
			chai.request(app)
				.post('/login')
				.send({
					email: 'coco@lasticot.com',
					password: '123456'
				})
				.end(function (err, res) {
					token = res.body.token;
					expect(err).to.be.null;
					expect(res).to.have.status(200);
					chai.request(app)
						.post('/read/game/wish')
						.send({
							token: token,
						})
						.end(function (err, res) {
							expect(err).to.be.null;
							expect(res).to.have.status(200);
							done();
							});
				});
		});
	});

	describe('#Delete', function() {
		it('should return status 200 and no err', (done) => {
			chai.request(app)
				.post('/login')
				.send({
					email: 'coco@lasticot.com',
					password: '123456'
				})
				.end(function (err, res) {
					token = res.body.token;
					expect(err).to.be.null;
					expect(res).to.have.status(200);
					chai.request(app)
						.post('/delete/game/wish')
						.send({
							token: token, 
							game_id: "321654987746516564",
						})
						.end(function (err, res) {
							expect(err).to.be.null;
							expect(res).to.have.status(200);
							done();
							});
				});
		});
	});
});


//OWNED LIST
describe('#OWNED LIST', function() {
	before(function(done) {
		chai.request(app)
			.post('/register')
			.send({
				username: 'cocolasticot',
				email: 'coco@lasticot.com',
				password: '123456',
				password_confirm: '123456'
			})
			.end(function (err, res) {
				done();
				});
	});

	after(function(done) {
		//Clean db
		mongoose.connect('mongodb://127.0.0.1:27017/GameBook', {useNewUrlParser: true});
		const connection = mongoose.connection;
		connection.once('open', function() {
		        console.log('MongoDB connection established successfully.')
			User.deleteOne({"email": "coco@lasticot.com"})
				.then(res => {
					List.deleteOne({"email": "coco@lasticot.com"})
						.then(res => {
							console.log('MongoDB cleaned before starting the tests.')
							done();
						});
				})
		});
	});

	describe('#Add', function() {
		it('should return status 200 and no err', (done) => {
			chai.request(app)
				.post('/login')
				.send({
					email: 'coco@lasticot.com',
					password: '123456'
				})
				.end(function (err, res) {
					token = res.body.token;
					expect(err).to.be.null;
					expect(res).to.have.status(200);
					chai.request(app)
						.post('/add/game/owned')
						.send({
							token: token, 
							game_id: "321654987746516564",
						})
						.end(function (err, res) {
							expect(err).to.be.null;
							expect(res).to.have.status(200);
							done();
							});
				});
		});
	});

	describe('#Delete', function() {
		it('should return status 200 and no err', (done) => {
			chai.request(app)
				.post('/login')
				.send({
					email: 'coco@lasticot.com',
					password: '123456'
				})
				.end(function (err, res) {
					token = res.body.token;
					expect(err).to.be.null;
					expect(res).to.have.status(200);
					chai.request(app)
						.post('/delete/game/owned')
						.send({
							token: token, 
							game_id: "321654987746516564",
						})
						.end(function (err, res) {
							expect(err).to.be.null;
							expect(res).to.have.status(200);
							done();
							});
				});
		});
	});
});
