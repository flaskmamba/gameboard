//IMPORTS
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const jsonwebtoken = require("jsonwebtoken");


//CONFIG
const app = express();
const userRoutes = express.Router();
const PORT = 5000;
app.use(bodyParser.json());


//MONGOOSE SCHEMA
const User = require('./models/userModel');
const List = require('./models/listModel');


//DATABASE CONFIG MONGODB
mongoose.connect('mongodb://127.0.0.1:27017/GameBook', { useNewUrlParser: true });
const connection = mongoose.connection;

connection.once('open', function () {
	console.log('MongoDB connection established successfully.')
});


//CONTROLLERS
const userHandlers = require('./controllers/userController');
const listHandlers = require('./controllers/listController');


//ROUTER
//USER ROUTES
app.route('/register')
	.post(userHandlers.register)

app.route('/login')
	.post(userHandlers.login);

app.route('/delete/user')
	.post(userHandlers.deleteUser);

app.route('/update/user')
	.post(userHandlers.updateUser);

app.route('/get/user')
	.post(userHandlers.getUser);

//LIST ROUTES
app.route('/add/game/wish')
	.post(listHandlers.addWish);

app.route('/delete/game/wish')
	.post(listHandlers.deleteWish);

app.route('/read/game/wish')
	.post(listHandlers.readWish);

app.route('/add/game/owned')
	.post(listHandlers.addOwned);

app.route('/delete/game/owned')
	.post(listHandlers.deleteOwned);
	
app.route('/read/game/owned')
	.post(listHandlers.readOwned);


//RUN
module.exports = app.listen(PORT, function () {
	console.log("Server is running on Port: " + PORT)
});
