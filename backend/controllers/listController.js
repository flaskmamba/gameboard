const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const User = mongoose.model('User');
const List = mongoose.model('List');
const Game = mongoose.model('Game');
const jwtSignature = '!!GameBook__SecretKey!!'; 
mongoose.set('useFindAndModify', false);

exports.addWish = function(req, res) {
	jwt.verify(req.body.token, jwtSignature, function(err, user) {
		if (err) {
			console.log(err)
	                res.status(400).json({'message': err.message});
		} else {
			List.find({user_id: user._id, wish: {$elemMatch: {game_id: req.body.game_id}}})
				.then(game => {
					if (game.length >= 1) {
						res.status(400).json({'message': 'This game is already in the wishlist.'});
					} else {
						var gameData = {
							game_id: req.body.game_id
						}
						var newGame = new Game(gameData);

						List.findOneAndUpdate({user_id: user._id}, {$push: {wish: newGame}})
						        .then(push => {
						                res.status(200).json({'message': 'Game added to wishlist successfully.'});
						        })
						        .catch(err => {
						                console.log(err)
						                res.status(400).json({'message': err.message});
						        });
					}
				})
			}
	})
};

exports.readWish = function(req, res) {
	jwt.verify(req.body.token, jwtSignature, function(err, user) {
		if (err) {
			console.log(err)
	                res.status(400).json({'message': err.message});
		} else {
			List.findOne({user_id: user._id})
			.then(wishList => {
				if (wishList) {
					res.status(200).json({
						'wishList': wishList 
					});
				} else {
					res.status(400).json({'msg': "User list not found."});
				}
			})
		}
	})	
}

exports.deleteWish = function(req, res) {
	jwt.verify(req.body.token, jwtSignature, function(err, user) {
		if (err) {
			console.log(err)
	                res.status(400).json({'message': err.message});
		} else {
			List.updateOne({user_id: user._id}, {$pull: {wish: {game_id: req.body.game_id}}})
				.then(game => {
					res.status(200).json({'message': 'Game deleted successfully from wishlist.'});
				})
				.catch(err => {
				                console.log(err)
				                res.status(400).json({'message': err.message});
				});
		}
	})
};

exports.addOwned = function(req, res) {
	jwt.verify(req.body.token, jwtSignature, function(err, user) {
		if (err) {
			console.log(err)
	                res.status(400).json({'message': err.message});
		} else {
			List.find({user_id: user._id, owned: {$elemMatch: {game_id: req.body.game_id}}})
				.then(game => {
					if (game.length >= 1) {
						res.status(400).json({'message': 'This game is already in the owned list.'});
					} else {
						var gameData = {
							game_id: req.body.game_id
						}
						var newGame = new Game(gameData);

						List.findOneAndUpdate({user_id: user._id}, {$push: {owned: newGame}})
						        .then(push => {
						                res.status(200).json({'message': 'Game added to owned list successfully.'});
						        })
						        .catch(err => {
						                console.log(err)
						                res.status(400).json({'message': err.message});
						        });
					}
				})
			}
	})
};

exports.deleteOwned = function(req, res) {
	jwt.verify(req.body.token, jwtSignature, function(err, user) {
		if (err) {
			console.log(err)
	                res.status(400).json({'message': err.message});
		} else {
			List.updateOne({user_id: user._id}, {$pull: {owned: {game_id: req.body.game_id}}})
				.then(game => {
					res.status(200).json({'message': 'Game deleted successfully from owned list.'});
				})
				.catch(err => {
				                console.log(err)
				                res.status(400).json({'message': err.message});
				});
		}
	})
};

exports.readOwned = function(req, res) {
	jwt.verify(req.body.token, jwtSignature, function(err, user) {
		if (err) {
			console.log(err)
	                res.status(400).json({'message': err.message});
		} else {
			List.findOne({user_id: user._id})
			.then(ownedList => {
				if (ownedList) {
					res.status(200).json({
						'ownedList': ownedList 
					});
				} else {
					res.status(400).json({'msg': "User list not found."});
				}
			})
		}
	})	
}