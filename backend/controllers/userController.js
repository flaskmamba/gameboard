const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = mongoose.model('User');
const List = mongoose.model('List');
const jwtSignature = '!!GameBook__SecretKey!!'; 

exports.register = function(req, res) {
	userData = {
	        username:	req.body.username,
	        email:		req.body.email,
	}
	var newUser = new User(userData);
	newUser.password = bcrypt.hashSync(req.body.password, 10);
	//Save new user
	newUser.save()
	        .then(user => {
			//Init list with user id
			let initList = new List({"user_id": user._id, "email": user.email});
			initList.save()
				.then(list => {
	                		res.status(200).json(user);
				})
				.catch(err => {
	                		res.status(400).json({'message': err.message});
				});

	        })
	        .catch(err => {
	                console.log(err)
	                res.status(400).json({'message': err.message});
	        });
};


exports.login = function(req, res) {
	User.findOne({
		email: req.body.email
	}, function(err, user) {
		if (err) throw err;
		if (!user) {
			res.status(401).json({ message: 'Authentication failed. User not found.' });
		} else if (user) {
			if (!user.comparePassword(req.body.password)) {
				res.status(401).json({ message: 'Authentication failed. Wrong password.' });
			} else {
				return res.status(200).json({token: jwt.sign({email: user.email, username: user.username, _id: user._id, admin: user.admin}, jwtSignature)});
			}
		}
	});
};


exports.loginRequired = function(req, res, next) {
	if (req.user) {
		next();
	} else {
		return res.status(401).json({ message: 'Unauthorized user!' });
	}
};


exports.deleteUser = function(req, res) {
	jwt.verify(req.body.token, jwtSignature, function(err, user) {
		if (err) {
			console.log(err)
	                res.status(400).json({'message': err.message});
		} else {
			if (user.email === req.body.email || user.admin === true) {
				User.deleteOne({
					'email': req.body.email
				}, function(err, del) {
					if (err) throw err;
					if (!del) {
						res.status(400).json({ message: 'Deletion failed.'});
					} else {
						res.status(200).json(del)
					}
				});
			} else {
				res.status(400).json({ message: 'Unauthorized operation or user not found.'});
			}
		}
	});
};

exports.updateUser = function(req, res) {
	jwt.verify(req.body.token, jwtSignature, function(err, user) {
		if (err) {
	                res.status(400).json({'message': err.message});
		} else {
			newData = {
			        username: req.body.new_username,
			        lastname: req.body.new_lastname,
			        firstname: req.body.new_firstname,
			        email: req.body.new_email,
			        description: req.body.new_description,
			        birthday: req.body.new_birthday,
			}
			var newUser = new User(newData);
			newUser.password = bcrypt.hashSync(req.body.new_password, 10);

			if (user.email === req.body.email || user.admin === true) {
				new_hash = bcrypt.hashSync(req.body.new_password, 10);
		        	User.updateOne({email: req.body.email}, {$set:{
                        		username: req.body.new_username,
                        		lastname: req.body.new_lastname,
                        		firstname: req.body.new_firstname,
                        		email: req.body.new_email,
                        		password: new_hash, 
                        		description: req.body.new_description,
                        		birthday: req.body.new_birthday
                		}}, function(err, update) {
					if (err) throw err;
					if (!update) {
						res.status(400).json({ message: 'Update failed.'});
					} else {
						res.status(200).json({token: jwt.sign({email: newUser.email, username: newUser.username, _id: user._id, admin: user.admin}, jwtSignature)});
					}
				});
			} else {
				res.status(400).json({ message: 'Unauthorized operation or user not found.'});
			}
		}
	});
};

exports.getUser = function(req, res) {
	jwt.verify(req.body.token, jwtSignature, function(err, user) {
		if (err) {
			res.status(400).json({'message': err.message});
		} else {
			if (user) {
				User.findOne({email: user.email})
				.then( user => {
        				if (user) {
        			        	res.status(200).json({
        			                	'user': user 
        			        	});
        				} else {
        			        	res.status(400).json({'msg': "User not found."});
					}
				})
			} else {
				res.status(400).json({ message: 'Unauthorized operation or user not found.'});
			}
		}
	})
};
