import React from 'react'
import { createStackNavigator, createAppContainer, createBottomTabNavigator, createSwitchNavigator } from 'react-navigation'
import { Icon } from 'react-native-elements'
import Login from '../components/login'
import Auth from '../components/auth'
import Register from '../components/register'
import Profile from '../components/profile'
import ShowProfile from '../components/showProfile'
import EditProfile from '../components/editProfile'
import History from '../components/history'
import Search from '../components/searchComponent'
import SearchResult from '../components/searchResult'
import GameDetail from '../components/gameDetail'
import WishList from '../components/wishList'
import OwnedList from '../components/ownedList'
import Lists from '../components/listsComponent'


// ***
const ProfileStack = createStackNavigator({

    Profile: {
        screen: Profile,
        navigationOptions: {
            title: "Profile",
        },
    },
    ShowProfile: {
        screen: ShowProfile,
        navigationOptions: {
            title: "ShowProfile",
        }
    },
    EditProfile: {
        screen: EditProfile,
        navigationOptions: {
            title: "EditProfile",
        }
    },
    Login: {
        screen: Login,
        navigationOptions: {
            title: "Login"
        }
    },
    Register: {
        screen: Register,
        navigationOptions: {
            title: "Register"
        }
    },
    Search: {
        screen: Search,
	navigationOptions: {
		headerTitleStyle: {
			fontFamily: 'Minecraft'
		}
	}
    },
    Auth: {
        screen: Auth,
        navigationOptions: {
            title: "Auth"
        }
    },
    WishList: {
        screen: WishList,
        navigationOptions: {
            title: "Your wish list"
        }
    },
    OwnedList: {
        screen: OwnedList,
        navigationOptions: {
            title: "Your owned list"
        }
    }
})

ProfileStack.navigationOptions = {
    tabBarLabel : 'Profile',
    tabBarIcon : (<Icon name="person" size={35} color={'#ae0b00'} />)
}


const HistoryStack = createStackNavigator({
    History: {
        screen: History,
        navigationOptions: {
            title: "Your rounds",
        }
    }
})

HistoryStack.navigationOptions = {
    tabBarLabel : 'History',
    tabBarIcon : (<Icon name="history" size={35} color={'#ae0b00'} />)
}

const ListStack = createStackNavigator({
    List: {
        screen: Lists,
        navigationOptions: {
            title: "Your Lists",
        }
    }
})

ListStack.navigationOptions = {
    tabBarLabel : 'Lists',
    tabBarIcon : (<Icon name="list" size={35} color={'#ae0b00'}/>)
}

const SearchStack = createStackNavigator({
    Search: {
        screen: Search,
        navigationOptions: {
            title: "Welcome to GameBook",
        }
    },
    SearchResult: {
        screen: SearchResult,
        navigationOptions: {
            title: "Your search results",
        }
    },
    GameDetail: {
        screen: GameDetail,
    },
    WishList: {
        screen: WishList,
        navigationOptions: {
            title: "Your wish list",
        }
    },
    OwnedList: {
        screen: OwnedList,
        navigationOptions: {
            title: "Your owned list",
        }
    }
})

SearchStack.navigationOptions = {
    tabBarLabel : 'Search',
    tabBarIcon : (<Icon name="search" size={35} color={'#ae0b00'} />),
}

const TabNavigator = createBottomTabNavigator({
    SearchStack,
    ProfileStack,
    ListStack,
    HistoryStack,
})


export default createAppContainer(createSwitchNavigator({TabNavigator,}));
