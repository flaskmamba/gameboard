import React from 'react'
import { View, Button, TouchableOpacity, Text, StyleSheet, ImageBackground } from 'react-native'
import Login from './login';
import Register from './register';
import { Font } from 'expo';

class Header extends React.Component {

  	state = {
  	  fontLoaded: false
  	};


    async componentDidMount() {
    	await Font.loadAsync({
      		'Minecraft': require('../assets/Minecraft.ttf')
	})

	this.setState({fontLoaded: true});
    }

    render() {
	    const {title} = this.props
        return (
	    this.state.fontLoaded ? (
            	<View style={styles.customHeader}>
	    		<Text style={styles.customText}>{title}</Text>
            	</View >
	    ) : null
        )
    }
}

const styles = StyleSheet.create({
    customHeader: {
        fontFamily: 'Minecraft',
	fontSize: 30,
	paddingTop: 30,
	paddingBottom: 10,
	backgroundColor: 'black',
	color: 'white',
    },
    customText: {
        fontFamily: 'Minecraft',
	fontSize: 30,
	paddingTop: 30,
	paddingBottom: 10,
	backgroundColor: 'black',
	color: 'white',
    },
});

export default Header 

