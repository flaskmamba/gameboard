import React from 'react'
import { FlatList, View, Text, ScrollView, StyleSheet } from 'react-native'
import games from './helpers/gameTitle'
import Game from './gameData.js'

class OwnedList extends React.Component {

  render() {
    return ( 
      <View>
        <FlatList
          data={games}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => <Game picture={item} displayGameDetail={this._displayGameDetail}/>}
        />
      </View>
    )}
}

export default OwnedList