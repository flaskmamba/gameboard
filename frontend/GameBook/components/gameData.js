import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'
import StarRating from 'react-native-star-rating'

export default class Game extends Component {

   imageExist(){
      if (!this.props.picture.thumb_url){
         return this.props.picture;
      } else {
         return this.props.picture.thumb_url;  
      }
   }

   render() {
      const pickData = this.props.picture;  
      const imageUrl = this.imageExist()    
      const displayGameDetail = this.props.displayGameDetail

      return (
         <TouchableOpacity 
          style={styles.main_container}
          onPress={() => displayGameDetail(pickData)}
        >

               <Image style={styles.image} source={{ uri: imageUrl }} />
               
               <View style={styles.content_container}>
               <View style={styles.header_container}>
                <Text style={styles.title_text} numberOfLines={1}>{pickData.name}</Text>
		<StarRating
                    rating={pickData.average_user_rating}
                    maxStars={4}
                    emptyStarColor='white'
                    fullStarColor='white'
                    starSize= {18}
                    style={styles.star}
                />
              </View>
          
               <View style={styles.description_container}>
                  <Text style={styles.description_text}>Players : {pickData.min_players} to {pickData.max_players}</Text>
                  <Text style={styles.description_text}>Playtime : {pickData.min_playtime} min to {pickData.max_playtime} min</Text>
               </View>
              
               </View>
         </TouchableOpacity>
      )

   }

}

const styles = StyleSheet.create({
    main_container: {
      marginTop: 10,
      height: 110,
      flexDirection: 'row', 
      backgroundColor: '#ae0b00',
      margin: 10,
      borderColor: 'white',
      borderWidth: 2,
      borderRadius: 8,
    },
    image: {
      width: 60,
      height: 90,
      margin: 10,
      backgroundColor: 'black'
    },
    content_container: {
      flex: 1,
    },
    header_container: {
      flex: 3,
      flexDirection: 'row',
    },
    title_text: {
      fontWeight: 'bold',
      fontSize: 18,
      flex: 1,
      color: 'black',
      marginLeft: 15,
      marginTop: 4,
    },
    star: {
      flex: 1,
      margin: 30,
      padding: 30
    },
    description_container: {
      flex: 7,
      marginLeft: 15
    },
    description_text: {
      marginTop: 8,
      fontSize: 14,
      color: 'white'
    },
  })


