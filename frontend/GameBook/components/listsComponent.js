import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, FlatList, ScrollView, AsyncStorage, ActivityIndicator } from 'react-native'
import { Icon } from 'react-native-elements'
import ListData from './listData'
const myIP = require('../myIP.json')
import Header from './header';

export default class Lists extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedList: [],
            isLoading: false,
            token: '',
        }
        this.setToken = this.setToken.bind(this)
    }

	static navigationOptions = {
		header: props => (
			<Header 
				{...props}
				title="Lists"
			/>
		)
	}

    async componentDidMount() {
        await this.setToken();
    }

    async setToken() {
        let token1 = await AsyncStorage.getItem('user');
        this.setState({
            token: token1
        })
    }

    async displayWishList() {
        this.setState({ isLoading: true });
        this.resetSelectedList();
        console.log('token' + this.state.token)
        await fetch('http://' + myIP.backIP + ':5000/read/game/wish', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'token': this.state.token,
            })
        })
            .then(res => {
                if (res.ok) {
                    var objt = JSON.parse(res._bodyInit);
                    this.setState({
                        selectedList: objt.wishList.wish,
                        isLoading: false,
                    })
                }
            })
    }

    async displayOwnedList() {
        this.setState({ isLoading: true });
        this.resetSelectedList();
        console.log('token' + this.state.token)
        await fetch('http://' + myIP.backIP + ':5000/read/game/owned', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'token': this.state.token,
            })
        })
            .then(res => {
                if (res.ok) {
                    var objt = JSON.parse(res._bodyInit);
                    this.setState({
                        selectedList: objt.ownedList.owned,
                        isLoading: false,
                    })
                }
            })
    }

    displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    resetSelectedList() {
        this.setState({
            selectedList: []
        })
    }

    render() {
        console.log("state : " + this.state.selectedList)
        return (
            <View style={styles.main_container}>
                <View style={styles.container_2}>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.displayWishList()}>
                        <Icon name="heart" type="antdesign" size={25} color={'white'} />
                        <Text style={styles.button_text} >Wish list</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.displayOwnedList()}>
                        <Icon name="checkcircle" type="antdesign" size={25} color={'white'} />
                        <Text style={styles.button_text}>Owned list</Text>
                    </TouchableOpacity>
                </View>
                
                <ScrollView>

                    <FlatList 
                        data={this.state.selectedList}
                        keyExtractor={(item) => item.game_id}
                        renderItem={({ item }) => <ListData list={item} />}
                    />
                </ScrollView>
                {this.displayLoading()}

            </View>
        )
    }
}


const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: 'black'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 35,
        flex: 1,
        flexWrap: 'wrap',
        margin: 10,
        color: 'white',
        textAlign: 'left'
    },
    container_2: {
        flex: 1,
        flexDirection: 'row'
    },
    button: {
        flex: 1,
        backgroundColor: '#ae0b00',
        width: 8,
        height: 60,
        borderRadius: 15,
        textAlign: 'center',
        justifyContent: 'center',
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: "white",
        padding: 15
    },
    button_text: {
        fontWeight: 'bold',
        color: 'white'
    }

})
