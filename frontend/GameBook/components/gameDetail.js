import React from 'react'
import { StyleSheet, View, Text, ScrollView, Image, TouchableOpacity, AsyncStorage } from 'react-native'
import SearchComponent from './searchComponent'
import { Icon } from 'react-native-elements'
import History from './history'
import WishList from './wishList'
import OwnedList from './ownedList';
const myIP = require('../myIP.json');

const Search = new SearchComponent

class GameDetail extends React.Component {

  constructor(props) {
    super(props)
    this.searchedText = "",
      // Initialisation de notre donnée searchedText en dehors du state
      this.accessToken = '',
      this.gameTitle = this.props.navigation.state.params.idGame.name,
      this.state = {
        game: undefined,
        isLoading: false,
        token : '',
      }
      this.setToken = this.setToken.bind(this)
  }

  async componentDidMount() {
    await this.setToken();
  }

  async setToken(){
    let token1 = await AsyncStorage.getItem('user');
    this.setState ({
      token : token1
    })
  }

  addToWishList() {
    fetch('http://'+myIP.backIP+':5000/add/game/wish',
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'token': this.state.token,
          'game_id': this.gameTitle,
        })
      })
      .then(
        alert("Added to your wishlist")
      )
      .catch((error)=>{
        console.log("addtoWishlist call error");
        alert(error.message);
      });
  }

  addToOwnedList() {
    fetch('http://'+myIP.backIP+':5000/add/game/owned',
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'token': this.state.token,
          'game_id': this.gameTitle,
        })
      })
      .then(
        alert("Added to your onwed list")
      )
      .catch((error)=>{
        console.log("addtoOwnedlist call error");
        alert(error.message);
     });
  }

  render() {
    return (
        <ScrollView style={styles.main_container}>
          <Text style={styles.title}>{this.props.navigation.state.params.idGame.name}</Text>
          <View style={styles.image_container}>
            <Image style={styles.image} source={{ uri: this.props.navigation.state.params.idGame.image_url }} />
          </View>
          <Text style={styles.description}>{this.props.navigation.state.params.idGame.description_preview}</Text>
          <Text style={styles.default}>Minimum players : {this.props.navigation.state.params.idGame.min_players}</Text>
          <Text style={styles.default}>Maximum players : {this.props.navigation.state.params.idGame.max_players}</Text>
          <Text style={styles.default}>Minimum playtime : {this.props.navigation.state.params.idGame.min_playtime}</Text>
          <Text style={styles.default}>Maximum playtime : {this.props.navigation.state.params.idGame.max_playtime}</Text>
          <Text style={styles.default}>Publishers : {this.props.navigation.state.params.idGame.publishers}</Text>
        
          <View style={styles.container_2}>
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={() => this.addToWishList()}>
                      <Icon name="heart" type="antdesign" size={25} color={'white'} />
                      <Text style={styles.button_text} >Want it</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={() => this.addToOwnedList()}>
                    <Icon name="checkcircle" type="antdesign" size={25} color={'white'} />
                    <Text style={styles.button_text}>Have it</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={() => this.props.navigation.navigate("History")}>
                    <Icon name="game-controller" type="entypo" size={25} color={'white'} />
                    <Text style={styles.button_text}>Add game</Text>
                </TouchableOpacity>
          </View>
        
        </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    backgroundColor: 'black'
  },
  image_container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 130,
    margin: 10,
    backgroundColor: 'black',
    borderWidth: 3,
    borderColor: "black",
  },
  title: {
    flex: 1,
    flexWrap: 'wrap',
    fontWeight: 'bold',
    fontSize: 35,
    color: '#ae0b00',
    textAlign: 'center',
    marginLeft: 20,
    color: 'white'
  },
  description: {
    fontStyle: 'italic',
    color: 'white',
    margin: 15,
    textAlign: 'center',
    fontSize: 18
  },
  default: {
    marginLeft: 20,
    marginRight: 5,
    marginTop: 5,
    color: 'white'
  },
  container_2: {
    flex: 1,
    flexDirection: 'row'
  },
  button: {
    flex: 1,
    backgroundColor: '#ae0b00',
    width: 10,
    height: 60,
    borderRadius: 15,
    textAlign: 'center',
    justifyContent: 'center',
    margin: 8,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: "white",
    padding: 15
  },
  button_text: {
    fontWeight: 'bold',
    color: 'white'
  }

})

export default GameDetail
