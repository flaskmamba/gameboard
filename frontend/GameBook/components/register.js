import React from 'react'
import { View, Button, TextInput, Image, ScrollView, Text, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native'
import { AsyncStorage } from 'react-native';
const myIP = require('../myIP.json');
import Header from './header';

export default class Register extends React.Component {

    constructor(props) {
        super(props);

        this.onChangeRegisterEmail = this.onChangeRegisterEmail.bind(this);
        this.onChangeRegisterPassword = this.onChangeRegisterPassword.bind(this);
        this.onChangeRegisterPasswordConfirm = this.onChangeRegisterPasswordConfirm.bind(this);
        this.onChangeRegisterUsername = this.onChangeRegisterUsername.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            username: '',
            password: '',
            password_confirm: '',
            email: '',
        }
    }
	static navigationOptions = {
		header: props => (
			<Header 
				{...props}
				title="Login"
			/>
		)
	}

    onChangeRegisterEmail(e) {
        this.setState({
            email: e.nativeEvent.text
        });
    }

    onChangeRegisterPassword(e) {
        this.setState({
            password: e.nativeEvent.text
        });
    }

    onChangeRegisterPasswordConfirm(e) {
        this.setState({
            password_confirm: e.nativeEvent.text
        });
    }

    onChangeRegisterUsername(e) {
        this.setState({
            username: e.nativeEvent.text
        });
    }


    onSubmit(e) {
        e.preventDefault();

        fetch('http://'+myIP.backIP+':5000/register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
                password_confirm: this.state.password_confirm,
                email: this.state.email,
            })
        })
            .then(res => {
                if (res.ok) {
                    this.props.navigation.navigate('Login')
                }
            })

    }

    render() {
        return (
            <ImageBackground source={{ uri: 'https://wallimpex.com/data/out/791/red-wallpaper-abstract-11382195.png' }} style={{ width: '100%', height: '100%' }}>
                <View style={styles.container}>
                    <TextInput style={styles.textinput}
                        placeholder='Username' onChange={this.onChangeRegisterUsername} />

                    <TextInput style={styles.textinput}
                        placeholder='Email' onChange={this.onChangeRegisterEmail} />

                    <TextInput style={styles.textinput}
                        placeholder='Password' 
                        onChange={this.onChangeRegisterPassword}
                        secureTextEntry={true} 
                         />

                    <TextInput style={styles.textinput}
                        placeholder='Confirm password' 
                        onChange={this.onChangeRegisterPasswordConfirm} 
                        secureTextEntry={true} 
                        />

                    <TouchableOpacity 
                        onPress={this.onSubmit}
                        style={styles.button} >
                    <Text style={styles.text}>Submit</Text>
                    </TouchableOpacity>
            </View>
                    </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor: '#ae0b00'
    },
    textinput: {
        margin: 15,
        height: 60,
        width: 300,
        borderColor: 'black',
        borderWidth: 4,
        padding: 15,
        borderRadius: 15,
        backgroundColor: 'white',
        fontSize: 18,
    },
    text: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
    },
    button: {
        backgroundColor: 'black',
        marginTop: 30,
        width : 200,
        borderRadius: 15,
        borderColor: 'white',
        borderWidth: 4,
        padding: 12,
        textAlign: 'center',
        overflow: 'hidden'
    }

});

