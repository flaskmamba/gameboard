import React from 'react'
import { View, TextInput, Image, ScrollView, StyleSheet, TouchableOpacity, Text } from 'react-native'
import { Icon } from 'react-native-elements'
import {AsyncStorage, ActivityIndicator} from 'react-native';
import Auth from './auth';
import ShowProfile from './showProfile';
import Header from './header';


class Profile extends React.Component {

	constructor(props) {
		super(props);

		this._displayProfile = this._displayProfile.bind(this);
		this._displayLoading = this._displayLoading.bind(this);
		this._getToken = this._getToken.bind(this);

		this.state = {
			isLoading: true,
			token: undefined 
			}
	}

	static navigationOptions = {
		header: props => (
			<Header 
				{...props}
				title="Profile"
			/>
		)
	}

	_displayLoading() {
		if (this.state.isLoading) {
			return (
				<View style={styles.loading_container}>
					<ActivityIndicator size='large' />
				</View>
			)
		}
	}

	async _getToken() {
		try {
			const token = await AsyncStorage.getItem('user');
			if (token) {
				if (this.state.token !== token || this.state.isLoading !== false) {
					this.setState({
						isLoading: false,
						token: token
					})
				} 
			} else {
				if (this.state.token !== undefined || this.state.isLoading !== false) {
					this.setState({
						isLoading: false,
						token:undefined 
					})
				}
			}
		} catch (error) {
			console.log(error)
		}
	}

	async componentDidMount() {
		await this._getToken()
	}

	_displayProfile() {
		this._getToken()
		if (this.state.token) {
		    return <ScrollView><ShowProfile token={this.state.token} navigation={this.props.navigation}/></ScrollView>
		} else {
			if (! this.state.isLoading)
		    		return <ScrollView style={styles.scrollableView}><Auth navigation={this.props.navigation}/></ScrollView>
		}
	}

    render() {
        return (
		<View>
			{this._displayProfile()}
			{this._displayLoading()}
		</View>
        )
    }
}

const styles = StyleSheet.create({
	loading_container: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 200,
		bottom: 0,
		alignItems: 'center',
		justifyContent: 'center'
	},
	scrollableView:{
        height: 800,
        backgroundColor: '#000',

    },
})

const mapStateToProps = (state) => {
	return {
		profile: state.profile
	}
}
export default Profile;
