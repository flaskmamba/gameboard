import React from 'react'
import { StyleSheet, View, TextInput, Button, FlatList, AsyncStorage, ActivityIndicator, TouchableOpacity, Text } from 'react-native'
import Game from './gameData.js'
import { black } from 'ansi-colors';
import { Font } from 'expo';
import Header from './header'

export default class Search extends React.Component {

    constructor(props) {
        super(props)
        this.searchedText = "",
            // Initialisation de notre donnée searchedText en dehors du state
            this.accessToken = '',
            this.state = {
                pictures: [],
                isLoading: false,
		fontLoaded: false,

            }
    }
	static navigationOptions = {
		header: props => (
			<Header 
				{...props}
				title="Welcome to GameBook"
			/>
		)
	}

    async componentDidMount() {
        this.gallerySearch();
    	await Font.loadAsync({
      		'Minecraft': require('../assets/Minecraft.ttf')
	})

	this.setState({fontLoaded: true});
    }

    async gallerySearch() {
        this.setState({ isLoading: true })
        //GET request 
        await fetch('https://www.boardgameatlas.com/api/search?limit=3&client_id=SB1VGnDv7M&order_by=popularity', {
            method: 'GET'
        })
            .then((response) => {
                var objt = JSON.parse(response._bodyInit)
                this.paramSetting(objt);
                return  objt
            })
            .catch((error) => {
                //Error 
                alert(JSON.stringify(error));
                console.error(error);
            });
    }

    paramSetting(objt){
        this.setState({
            pictures: objt.games,
            isLoading: false
        })
    }

    displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    searchPicture(text) {
        this.searchedText = text;
        return this.searchedText;
        // Modification du texte recherché à chaque saisie de texte, sans passer par le setState comme avant
    }

    newSearch() {
        this.setState({
            pictures: []
        })
        this.gallerySearch()
        //this.props.navigation.navigate("SearchResult", { gameSearch: this.state.pictures })
    }

    searchResult() {
        this.props.navigation.navigate("SearchResult", { gameSearch: this.searchedText });
    }

    _displayGameDetail = (idGame) => {
        this.props.navigation.navigate("GameDetail", { idGame: idGame })
    }

    _getGameDetail(id) {
       const url = 'https://www.boardgameatlas.com/api/search?ids=' + id + '&client_id=P8HIpFWlpv'
       return fetch(url)
       .then((response) => {
           var objt = JSON.parse(response._bodyInit)
           this.setState({
               pictures: objt.games
           })
       })
       .catch((error) => console.error(error));
   }

    render() {
        return (
	    this.state.fontLoaded ? (
            	<View style={styles.main_container}>
                	<View style={styles.searchBox}>
                	    <TextInput style={styles.textInputSearch}
                	        onSubmitEditing={() => this.searchResult()}
                	        placeholder='Search a game'
                	        onChangeText={(text) => this.searchPicture(text)}
                	    />
                	</View>

                	<View style={styles.search}>
                	    <TouchableOpacity style={styles.button}
                	        onPress={() => this.searchResult()}>
                	        <Text style={styles.text}>SEARCH</Text>
                	    </TouchableOpacity>
                	</View>


                	<View style={styles.componentBox}>
                	    <FlatList
                	        data={this.state.pictures}
                	        keyExtractor={(item) => item.id.toString()}
                	        renderItem={({ item }) => <Game picture={item} displayGameDetail={this._displayGameDetail}/>}
                	    />
                	</View>
            	</View>
		) : null
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
	fontFamily: 'Minecraft',
        backgroundColor: 'black'
    },
    button_container: {
        alignItems: 'center',
    },
    button: {
        backgroundColor: '#ae0b00',
        padding: 14,
        margin: 10,
        borderRadius: 8,
        borderColor: 'white',
        borderWidth: 3,
        textAlign: 'center',
        marginBottom: 28,
	fontFamily: 'Minecraft',
        width: 200,
    },
    search: {
        justifyContent: 'center',
        alignItems : 'center',
    },
    componentBox: {
        flex: 1,
        justifyContent: 'center',
    },
    SearchBox: {
        justifyContent :'center',
        alignItems : 'center',
    },
    text: {
        color: 'white',
        fontWeight: 'bold',
	fontFamily: 'Minecraft',
        textAlign: 'center'
    },
    textInputSearch: {
        textAlign: 'center',
        backgroundColor: 'white',
        flexBasis:80,
        fontSize: 15,
        borderColor: '#ae0b00',
        borderWidth: 1,
        borderRadius: 8,
        margin: 10,
        padding: 10,
    },
})
