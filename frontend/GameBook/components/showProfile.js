import React from 'react'
import { View, TextInput, Image, ScrollView, StyleSheet, TouchableOpacity, Text } from 'react-native'
import { Icon } from 'react-native-elements'
import {AsyncStorage, ActivityIndicator} from 'react-native';
import EditProfile from './editProfile';
const myIP = require('../myIP.json');

class ShowProfile extends React.Component {

	constructor(props) {
		super(props);

		this.onLogout = this.onLogout.bind(this);
		this.onEdit = this.onEdit.bind(this);
		this._displayProfile = this._displayProfile.bind(this);

		this.state = {
			isLoading: true,
			user: undefined
			}

	}

	_displayLoading() {
		if (this.state.isLoading) {
			return (
				<View style={styles.loading_container}>
					<Text>Loading profile</Text>
					<ActivityIndicator size='large' />
				</View>
			)
		}
	}

	async onLogout() {
		try {
			await AsyncStorage.removeItem('user');
			this.props.navigation.navigate('Profile', {refresh: true})
		} catch (err) {
			console.log(err)
		}
	}

	onEdit() {
		this.props.navigation.navigate('EditProfile', {user: this.state.user, token: this.props.token})
	}



	componentDidMount() {
        	fetch('http://'+myIP.backIP+':5000/get/user', {
        	    method: 'POST',
        	    headers: {
        	        'Accept': 'application/json',
        	        'Content-Type': 'application/json'
        	    },
        	    body: JSON.stringify({
        	        'token': this.props.token,
        	    })
        	})
        	    .then(res => {
        	        if (res.ok) {
				let user = JSON.parse(res._bodyInit).user
				this.setState({
					isLoading: false,
					user: user 
				})
        	        }
        	    })
	}

	_displayProfile() {
		if (this.state.isLoading === false) {
			return <ScrollView style={styles.container}>
            			<Image source={{uri: 'https://i.kym-cdn.com/photos/images/newsfeed/001/077/564/d5b.jpg'}} style={styles.avatar} />
				<Text style={styles.text}>First Name: {this.state.user.firstname}</Text>
				<Text style={styles.text}>Last Name: {this.state.user.lastname}</Text>
				<Text style={styles.text}>Username: {this.state.user.username}</Text>
				<Text style={styles.text}>Email: {this.state.user.email}</Text>
				<Text style={styles.text}>Description: {this.state.user.description}</Text>
				<Text style={styles.text}>Member Since: {this.state.user.creation_date}</Text>
				<Text style={styles.text}>Birthday: {this.state.user.birthday}</Text>
               			 <View style={styles.container_2}>
               			 <TouchableOpacity 
               			     style={styles.button} 
               			     onPress={this.onEdit}>
               			         <Icon name="edit" type="entypo" size={25} color={'white'} />
               			 </TouchableOpacity>
               			 <TouchableOpacity 
               			     style={styles.button} 
               			     onPress={this.onLogout}>
               			     <Icon name="logout" type="antdesign" size={25} color={'white'} />
               			 </TouchableOpacity>
               			 </View>
				</ScrollView>
		}

	}

    render() {
        return (
		<View>
			{this._displayLoading()}
			{this._displayProfile()}
		</View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        //alignItems: 'center',
        //justifyContent: 'center'
	height: 700,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "#af0000",
        margin: 40,
        alignSelf:'center',
      },
    container_2: {
        flex: 1,
        flexDirection: 'row'
    },
    button: {
        flex: 1,
        backgroundColor: '#ae0b00',
        width : 10,
        height: 50,
        borderRadius: 15,
        textAlign: 'center',
        justifyContent: 'center',
        margin: 15,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: "white",
    },    
	loading_container: {
		marginTop: 200,
		alignItems: 'center',
		justifyContent: 'center'
    
	},
	text: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 14,
		marginLeft: 15,
	},
})
  
export default ShowProfile

