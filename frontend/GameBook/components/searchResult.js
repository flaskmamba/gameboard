import React from 'react'
import { StyleSheet, View, TextInput, Button, FlatList, AsyncStorage, ActivityIndicator } from 'react-native'
import Game from './gameData.js'
import Header from './header';

export default class SearchResult extends React.Component{
    constructor(props){
        super(props)
        this.searchedText = "",
        this.state = {
            pictures: [],
            isLoading: false,
        }
    }

    componentDidMount(){
        this.loadSearchGame()
        this.gallerySearch();
    }

	static navigationOptions = {
		header: props => (
			<Header 
				{...props}
				title="Search Results"
			/>
		)
	}

    loadSearchGame(){
        this.searchedText = this.props.navigation.state.params.gameSearch
    }

    async gallerySearch() {
        this.setState({ isLoading: true })
        if (this.searchedText.length > 0) {
            //GET request 
            await fetch('https://www.boardgameatlas.com/api/search?name='+ this.searchedText+"&client_id=P8HIpFWlpv", {
                method : 'GET'
            })
                .then((response) => {
                    var objt = JSON.parse(response._bodyInit)
                    // console.log("name : " + objt.games[0].name)

                    this.setState({
                        pictures: objt.games,
                        isLoading: false
                    })
                    //console.log(this.state.pictures)
                })
        
                .catch((error) => {
                    //Error 
                    alert(JSON.stringify(error));
                    console.error(error);
                });
        }
    }

    displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    _displayGameDetail = (idGame) => {
       this.props.navigation.navigate("GameDetail", { idGame: idGame })
   }

    

    render(){
        return(
            <View>
                <FlatList
                    data={this.state.pictures}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({ item }) => <Game picture={item} displayGameDetail={this._displayGameDetail}/>}
                />

                {this.displayLoading()}

            </View>
        )
    }
        
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        //margin: 5,
    },
    textinput: {
        margin: 30,
        padding: 5,
        height: 50,
        width: 300,
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
	backgroundColor: 'black'
    }


})
