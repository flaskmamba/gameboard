import React from 'react'
import { View, Button, TouchableOpacity, Text, StyleSheet, ImageBackground } from 'react-native'
import Login from './login';
import Register from './register';

class Auth extends React.Component {


    render() {
        return (
            <View style={styles.container}>

                <ImageBackground source={{ uri: 'https://wallimpex.com/data/out/791/red-wallpaper-abstract-11382195.png' }} style={{ width: '100%', height: 600 }}>

                    <View style={styles.buttonContainer}>
                        <TouchableOpacity
				style={styles.button}
				onPress={() => this.props.navigation.navigate("Login")}>
                            <Text style={styles.text}>Login</Text>
                        </TouchableOpacity>

                        <TouchableOpacity 
                            style={styles.button} 
                            onPress={() => this.props.navigation.navigate("Register")}>
                            <Text style={styles.text}>Register</Text>
                        </TouchableOpacity>
                    </View>

                </ImageBackground>
            </View >

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
    },
    input: {
        backgroundColor: 'white',
        width: 300,
        height: 30,
        borderRadius: 8,
        fontSize: 15
    },
    button: {
        backgroundColor: 'black',
        width : 200,
        height: 100,
        borderRadius: 15,
        borderRadius: 15,
        borderColor: 'white',
        borderWidth: 4,
        fontWeight: 'bold',
        overflow: 'hidden',
        padding: 12,
        textAlign: 'center',
        marginTop: 40,
        marginBottom: 40,
        justifyContent: 'center',
    }
});

export default Auth 

