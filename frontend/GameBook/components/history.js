import React from 'react'
import Dialog from "react-native-dialog"
import { View, Button, TextInput, Image, ScrollView, Text, StyleSheet, TouchableOpacity,  } from 'react-native'
import { AsyncStorage } from 'react-native';
import { Icon } from 'react-native-elements'
import Header from './header';

export default class History extends React.Component {

    //************** DIALOG ***************
    state = {
        dialogVisible: false
      };
 
      showDialog = () => {
        this.setState({ dialogVisible: true });
      };
 
      handleCancel = () => {
        this.setState({ dialogVisible: false });
      };
 
      handleAdd = () => {
        // The user has pressed the "Add" button, so here you can do your own logic.
        // ...Your logic
        this.setState({ dialogVisible: false });
      };
    //************** DIALOG ***************
   
	static navigationOptions = {
		header: props => (
			<Header 
				{...props}
				title="My Rounds"
			/>
		)
	}
    
    render() {
        return (
            <View style={styles.container} >
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={this.showDialog}>
                    <Icon name="game-controller" type="entypo" size={25} color={'white'} />
                    <Text style={styles.button_text}>Add game</Text>
                </TouchableOpacity>
           
                <Dialog.Container visible={this.state.dialogVisible}>
                    <Dialog.Title>Add Game</Dialog.Title>
                        <Dialog.Input style={styles.diag_input} placeholder="Game name"></Dialog.Input>
                        <Dialog.Input style={styles.diag_input} placeholder="Players"></Dialog.Input>
                        <Dialog.Input style={styles.diag_input} placeholder="Playing time"></Dialog.Input>
                        <Dialog.Input style={styles.diag_input} placeholder="Game date"></Dialog.Input>
                        <Dialog.Input style={styles.diag_input} placeholder="Score"></Dialog.Input>
                    <Dialog.Button label="ADD" onPress={this.handleAdd} />
                    <Dialog.Button label="Cancel" onPress={this.handleCancel} />
                </Dialog.Container>
         
         </View>
    )}


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
    },
    button: {
        backgroundColor: '#ae0b00',
        width : 120,
        height: 70,
        borderRadius: 15,
        textAlign: 'center',
        justifyContent: 'center',
        margin: 15,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 3,
        borderColor: "white",
        padding: 15,
        marginTop: 20
    },
    button_text: {
      fontWeight: 'bold',
      color: 'white'
    },
    diag_input: {
        backgroundColor: '#f7f7f7',
        borderRadius: 8
    }
});
