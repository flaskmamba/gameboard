import React from 'react'
import { Flatlist, View, Text, ScrollView, StyleSheet } from 'react-native'

class WishList extends React.Component {

  render() {
    return ( 
      <View style={styles.container}>
        <ScrollView>
          <Text>Wish List Component</Text>
        </ScrollView>
      </View>
    )}
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  }
});

export default WishList