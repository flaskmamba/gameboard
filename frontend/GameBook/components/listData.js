import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'

export default class ListData extends Component {


    render() {
        const {list} = this.props;
        console.log('list: '+list)
        return (
            <View>
                <Text style={styles.title}> {list.game_id} </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: '#af0000'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        margin: 10,
        color: 'white',
        textAlign: 'left'
    },

})