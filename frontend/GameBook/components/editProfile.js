import React from 'react'
import { View, TextInput, Image, ScrollView, StyleSheet, TouchableOpacity, Text } from 'react-native'
import { Icon } from 'react-native-elements'
import {AsyncStorage, ActivityIndicator} from 'react-native';
const myIP = require('../myIP.json');
import Header from './header';

class EditProfile extends React.Component {

    constructor(props) {
        super(props);

        this.onChangeUsername= this.onChangeUsername.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeFirstname = this.onChangeFirstname.bind(this);
        this.onChangeLastname = this.onChangeLastname.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onDelete= this.onDelete.bind(this);

	const { user } = this.props.navigation.state.params;

        this.state = {
	    username: user.username,
	    email: user.email,
	    new_email: user.email,
	    password: '',
	    firstname: user.firstname,
	    lastname: user.lastname,
	    description: user.description
        }
    }
	static navigationOptions = {
		header: props => (
			<Header 
				{...props}
				title="Edit Profile"
			/>
		)
	}

    onChangeUsername(e) {
        this.setState({
            username: e.nativeEvent.text
        });
    }
    onChangeEmail(e) {
        this.setState({
            new_email: e.nativeEvent.text
        });
    }
    onChangePassword(e) {
        this.setState({
            password: e.nativeEvent.text
        });
    }
    onChangeFirstname(e) {
        this.setState({
            firstname: e.nativeEvent.text
        });
    }
    onChangeLastname(e) {
        this.setState({
            lastname: e.nativeEvent.text
        });
    }
    onChangeDescription(e) {
        this.setState({
            description: e.nativeEvent.text
        });
    }

    onSubmit(e) {
        e.preventDefault();
	var body = JSON.stringify({
		token: this.props.navigation.state.params.token,
		email: this.state.email,
		new_username: this.state.username, 
		new_lastname: this.state.lastname, 
		new_firstname: this.state.firstname,
		new_email: this.state.new_email,
		new_password: '',
		new_description: this.state.description
            })

        fetch('http://'+myIP.backIP+':5000/update/user', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: body
            })
            .then(res => {
                if (res.ok) {
			this.props.navigation.navigate('Profile', {refresh: true})
                }
            })
	    .catch(err => {
		    console.log(err)
	    })
    }

    onDelete(e) {
	    console.log('delete')
        e.preventDefault();
	var body = JSON.stringify({
		token: this.props.navigation.state.params.token,
		email: this.state.email
	})

        fetch('http://'+myIP.backIP+':5000/delete/user', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: body
            })
            .then(res => {
                if (res.ok) {
			this.props.navigation.navigate('Profile', {refresh: true})
                }
            })
	    .catch(err => {
		    console.log(err)
	    })
    }

    render() {
	    const { user } = this.props.navigation.state.params;

        return (
            <View style={styles.container}>
                <ScrollView>
                <Image source={{uri: 'https://i.kym-cdn.com/photos/images/newsfeed/001/077/564/d5b.jpg'}} style={styles.avatar} />
                <TextInput
			placeHolder='Username'
			value={this.state.username}
			onChange={this.onChangeUsername}
			style={styles.input}
		/>

                <TextInput
			placeHolder='Email'
			value={this.state.new_email}
			onChange={this.onChangeEmail}
			style={styles.input}
		/>

                <TextInput
			placeHolder='New Password'
			value={this.state.password}
			onChange={this.onChangePassword}
                    	secureTextEntry={true}
			style={styles.input}
		/>

                <View style={styles.container_2}>
                    <TextInput
			value={this.state.firstname}
			onChange={this.onChangeFirstname}
			placeholder='First name'
			style={styles.input}
			/>
                    <TextInput
			value={this.state.lastname}
			onChange={this.onChangeLastname}
			placeholder='Last name'
			style={styles.input}
			/>
                </View>

                <TextInput 
			value={this.state.description}
			onChange={this.onChangeDescription}
                    	placeholder='Description'
                    	style={styles.input} 
                />
                <View style={styles.container_2}>
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={this.onDelete}>
                    <Icon name="delete" type="antdesign" size={25} color={'white'} />
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={this.onSubmit}>
                    <Icon name="update" type="MaterialCommunityIcons" size={25} color={'white'} />
                </TouchableOpacity>
                </View>
                </ScrollView> 
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "#af0000",
        margin: 40,
        alignSelf:'center',
      },
    container_2: {
        flex: 1,
        flexDirection: 'row'
    },
    input: {
        flex: 1,
        backgroundColor: 'white', 
        width: 300,
        height: 50,
        borderRadius: 8,
        fontSize: 15,
        margin: 10,
        padding: 5,
        borderWidth: 2,
        borderColor: "#ae0b00",
    },
    button: {
        flex: 1,
        backgroundColor: '#ae0b00',
        width : 10,
        height: 50,
        borderRadius: 15,
        textAlign: 'center',
        justifyContent: 'center',
        margin: 15,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: "white",
    },    
	loading_container: {
		marginTop: 200,
		alignItems: 'center',
		justifyContent: 'center'
    
	}
})
  
export default EditProfile
