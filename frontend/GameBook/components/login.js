import React from 'react'
import { View, Button, TextInput, Image, ScrollView, Text, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native'
import {AsyncStorage} from 'react-native';
const myIP = require('../myIP.json');
import Header from './header';

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.onChangeLoginEmail = this.onChangeLoginEmail.bind(this);
        this.onChangeLoginPassword = this.onChangeLoginPassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this._storeToken = this._storeToken.bind(this);

        this.state = {
            password: '',
            email: ''
        }
    }
	static navigationOptions = {
		header: props => (
			<Header 
				{...props}
				title="Login"
			/>
		)
	}

    async _storeToken(token) {
        try {
            await AsyncStorage.setItem('user', token);
            this.props.navigation.navigate('Profile', { refresh: true })
        } catch (err) {
            console.log(err)
        }
    };

    onChangeLoginEmail(e) {
        this.setState({
            email: e.nativeEvent.text
        });
    }

    onChangeLoginPassword(e) {
        this.setState({
            password: e.nativeEvent.text
        });
    }

    onSubmit(e) {
        e.preventDefault();
        fetch('http://'+myIP.backIP+':5000/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'password': this.state.password,
                'email': this.state.email
            })
        })
            .then(res => {
                if (res.ok) {
                    //console.log('back res: ', JSON.parse(res._bodyInit).token)
                    let token = JSON.parse(res._bodyInit).token;
                    this._storeToken(token)
                }
            })
    }

    render() {
        return (
            <ImageBackground source={{ uri: 'https://wallimpex.com/data/out/791/red-wallpaper-abstract-11382195.png' }} style={{ width: '100%', height: '100%' }}>
            <View style={styles.container}>
                <TextInput style={styles.textinput}
                    placeholder='Email' onChange={this.onChangeLoginEmail} />

                <TextInput style={styles.textinput}
                    placeholder='Password'
                    secureTextEntry={true}
                    onChange={this.onChangeLoginPassword}
                />

                <TouchableOpacity
                    onPress={this.onSubmit}
                    style={styles.button} >
                    <Text style={styles.text}>Submit</Text>
                </TouchableOpacity>
            </View>
                </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor: '#ae0b00'
    },
    title: {
        margin: 20,
        fontSize: 27,
        color: 'white'
    },
    textinput: {
        margin: 15,
        height: 60,
        width: 300,
        borderColor: 'black',
        borderWidth: 4,
        padding: 15,
        borderRadius: 15,
        backgroundColor: 'white',
        fontSize: 18,
    },
    text: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
    },
    button: {
        backgroundColor: 'black',
        marginTop: 30,
        width: 200,
        borderRadius: 15,
        borderColor: 'white',
        borderWidth: 4,
        padding: 12,
        textAlign: 'center',
        overflow: 'hidden',
        color: 'white'
    }
});

export default Login
