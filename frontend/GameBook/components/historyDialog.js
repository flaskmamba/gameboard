import Dialog from "react-native-dialog";
import {
   StyleSheet,
   Text,
   View,
   Image,
   DialogContent,
   style,
   TouchableOpacity,
   TextInput
 } from 'react-native';
 import React, {Component} from 'react';

 export default class Dialogue extends Component {
   state = {
       dialogVisible: false
     };

     showDialog = () => {
       this.setState({ dialogVisible: true });
     };

     handleCancel = () => {
       this.setState({ dialogVisible: false });
     };

     handleAdd = () => {
       // The user has pressed the "Add" button, so here you can do your own logic.
       // ...Your logic
       this.setState({ dialogVisible: false });
     };

     render() {
       return (
         <View>
             <TextInput style={styles.inputBox}
        underLIneColorAndroid='rgba(0,0,0,0)'
        placeholder="Name of tha Game"
        placeholderTextColor = "#ffffff"



        />
          <TextInput style={styles.inputBox}
        underLIneColorAndroid='rgba(0,0,0,0)'
        placeholder="Number of players"
        placeholderTextColor = "#ffffff"

        />
          <TextInput style={styles.inputBox}
        underLIneColorAndroid='rgba(0,0,0,0)'
        placeholder="Playing time"
        placeholderTextColor = "#ffffff"

        />
        <TextInput style={styles.inputBox}
        underLIneColorAndroid='rgba(0,0,0,0)'
        placeholder="Date playing Game"
        placeholderTextColor = "#ffffff"

        />
        <TextInput style={styles.inputBox}
        underLIneColorAndroid='rgba(0,0,0,0)'
        placeholder="score"
        placeholderTextColor = "#ffffff"
        />
           <TouchableOpacity onPress={this.showDialog}style={[styles.Button, style]}>
             <Text style={styles.Text}>Add Game</Text>
           </TouchableOpacity>
           <Dialog.Container visible={this.state.dialogVisible}>
             <Dialog.Title>Add Game</Dialog.Title>
             <Dialog.Description>
               Do you want to add this Game?
             </Dialog.Description>
             <Dialog.Button label="Cancel" onPress={this.handleCancel} />
             <Dialog.Button label="Add" onPress={this.handleDelete} />
           </Dialog.Container>
         </View>
       );
     }
   }
   const styles = StyleSheet.create({

       inputBox:{
           width:200,
           backgroundColor: "#8b0000",
           borderRadius:25,
           paddingHorizontal:16,
           fontSize:16,
           color:"#ffffff",
           marginVertical: 10
            },

       Text: {
               fontSize: 16,
               textAlign: 'center',
               color:"#ffffff",

           },

       Button: {
           backgroundColor:  "#8b0000",
           borderRadius: 20,
           padding: 10,
           margin: 5
        },

});